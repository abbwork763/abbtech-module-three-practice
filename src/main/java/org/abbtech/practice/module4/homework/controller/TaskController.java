package org.abbtech.practice.module4.homework.controller;


import lombok.extern.slf4j.Slf4j;
import org.abbtech.practice.module4.homework.entity.Task;
import org.abbtech.practice.module4.homework.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks")
@Slf4j
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping
    public List<Task> getAllTasks() {
        return taskService.getAllTasks();
    }

    @GetMapping("/{id}")
    public Task getTaskById(@PathVariable("id") Long id) {
        log.info("given task is {}", taskService.getTaskById(id));
        return taskService.getTaskById(id).orElse(null);
    }

    @PostMapping
    public Task saveTask(@RequestBody Task task) {
        return taskService.saveTask(task);
    }

    @PutMapping("/{id}")
    public Task updateTask(@PathVariable("id") Long id, @RequestBody Task task) {
        task.setId(id);
        return taskService.updateTask(task);
    }

    @DeleteMapping("/{id}")
    public void deleteTask(@PathVariable("id")Long id) {
        taskService.deleteTask(id);
    }

    @DeleteMapping
    public void deleteTasks(@RequestBody List<Long> ids) {
        taskService.deleteTasks(ids);
    }
}
