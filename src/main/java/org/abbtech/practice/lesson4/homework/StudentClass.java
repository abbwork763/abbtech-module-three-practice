package org.abbtech.practice.lesson4.homework;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "StudentClass")
public class StudentClass {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable=false, updatable=false)
    private int studentClassID;


    @ManyToOne
    @JoinColumn(name = "StudentID")
    private Student student;

}