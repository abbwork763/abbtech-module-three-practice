package org.abbtech.practice.lesson4.homework;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name = "Class")
public class Class {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ClassID")
    private int classID;

    @Column(name = "ClassName")
    private String className;

    @ManyToMany(mappedBy = "classes")
    private List<Student> students;

}